﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.Linq;
using System.Web;

namespace WebApplication1 {
    public class SQLSearch {

        /* Function Username
         * 
         * Discription: SQL query for DB search using a username.
         * This function is dedicated to Usernames ONLY. DO NOT USE FOR ANYTHING ELSE
         * 
         * input: SQL String (NOTE: must contain Username = @u and no other variables in the Query String!), string containing username  
         * output: DataSet containing the Query 
         */
        public static DataSet Username(string SQLQuery, string username) {
            //Open A SQL connection
            SqlCeConnection con = new SqlCeConnection("Data Source=|DataDirectory|\\Database1.sdf; Persist Security Info=False");
            con.Open();

            // Run a SQL query to get a list of the Messages and copy to a Data Set
            SqlCeCommand cmd = new SqlCeCommand(SQLQuery, con);
            cmd.Parameters.AddWithValue("@u", username);
            SqlCeDataAdapter da = new SqlCeDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            // Close the SQL DB connection
            con.Close();

            return ds;
        }

        /* Function ClearMessageOrBlock
         * 
         * Discription: SQL query to clear a Message Box or Blocked User
         * This function is dedicated to Clearing Messages or Blocked Users ONLY. DO NOT USE FOR ANYTHING ELSE
         * 
         * input: SQL String (NOTE: must contain Username = @u  and @b in the Query String)
         * output: none 
         */
        public static void ClearMessageOrBlock(string SQLQuery, string username) {
            //Open A SQL connection and do a query obtaining the all of the Users details using the logged in username
            SqlCeConnection con = new SqlCeConnection("Data Source=|DataDirectory|\\Database1.sdf; Persist Security Info=False");
            con.Open();

            SqlCeCommand cmd = new SqlCeCommand(SQLQuery, con);
            cmd.Parameters.AddWithValue("@u", username);
            cmd.Parameters.AddWithValue("@b", "");
            cmd.ExecuteNonQuery();

            // Close the SQL DB connection
            con.Close();
        }

        /* Function BasicSearch
         * 
         * Discription: SQL query for DataBase search using an Variable.
         *              This function is designed for overloading in future releases. To be used for most SQL queries. 
         * 
         * input: SQL String (NOTE: must contain Username = @u and no other variables in the Query String!), string containing value that is to be searched in the DataBase.  
         * output: DataSet containing the Query 
         */
        // add comments - similar to User search, but will have overloads
        public static DataSet BasicSearch(string SQLQuery, string SearchStringValue1) {
            //Open A SQL connection
            SqlCeConnection con = new SqlCeConnection("Data Source=|DataDirectory|\\Database1.sdf; Persist Security Info=False");
            con.Open();

            // Run a SQL query to get a list of the Messages and copy to a Data Set
            SqlCeCommand cmd = new SqlCeCommand(SQLQuery, con);
            cmd.Parameters.AddWithValue("@value", SearchStringValue1);
            SqlCeDataAdapter da = new SqlCeDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            // Close the SQL DB connection
            con.Close();

            return ds;
        }

    }
}