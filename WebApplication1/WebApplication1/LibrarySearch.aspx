﻿<%@ Page Title="Library Search" Language="C#"  AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="~/LibrarySearch.aspx.cs" Inherits="WebApplication1.LibrarySearch" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <hgroup class="title">
        <h1><%: Title %> </h1>       
    </hgroup>


    <a href="https://www.worldcat.org/libraries">
        <img src="images/Untitled1%20-%20Copy.png" alt="" border="0" style="height: 62px" />
    </a>

    <br />
    <p>
        <b><u>Information for WorldCat Library Search:</u></b>
        <br />
        &quot;WorldCat is the world&#39;s largest network of library content and services. WorldCat libraries are dedicated to providing access to their resources on the Web, where most people start their search for information.&quot; Because of this you are able to find popular booksm musics, CDS and videos - all of the physical items you&#39;re used to getting from libraries and bookstores.
</p>
    <p>
        <asp:TreeView ID="TreeView1" runat="server" style="margin-right: 0px">
            <Nodes>
                <asp:TreeNode Text="Search Help" Value="Search Help" SelectAction="None">
                    <asp:TreeNode Text="Searching by Libraries" Value="Searching by Libraries" Expanded="False" SelectAction="None">
                        <asp:TreeNode Text="To search for libraries around your location enter in the library name or part of a name, zip/postal code, state or province into the search bar." Value="To search for libraries around your location enter in the library name or part of a name, zip/postal code, state or province into the search bar." SelectAction="None"></asp:TreeNode>
                        <asp:TreeNode ImageUrl="images/WorldCat1.png" SelectAction="None"></asp:TreeNode>
                        <asp:TreeNode Text="Click the search button after entering the search details." Value="Click the search button after entering the search details." SelectAction="None"></asp:TreeNode>
                        <asp:TreeNode ImageUrl="images/WorldCat2.png" SelectAction="None"></asp:TreeNode>
                        <asp:TreeNode Text="View the library details through the website, or even map the library's location by Google maps through WorldCat map it feature." Value="View the library details through the website, or even map the library's location by Google maps through WorldCat map it feature." SelectAction="None"></asp:TreeNode>
                        <asp:TreeNode ImageUrl="images/WorldCat3.png" SelectAction="None"></asp:TreeNode>
                        <asp:TreeNode ImageUrl="images/WorldCat4.png" SelectAction="None"></asp:TreeNode>
                    </asp:TreeNode>
                    <asp:TreeNode Text="Searching by Items" Value="New Node" Expanded="False" SelectAction="None">
                        <asp:TreeNode Text="To search for library items enter in the a title, subject or author. Optional you are able to filter searchers through the advanced search option." Value="To serach for library items enter in the a title, subject or author. Optional you are able to filter searchers through the advanced search option." SelectAction="None"></asp:TreeNode>
                        <asp:TreeNode ImageUrl="images/WorldCat5.png" SelectAction="None"></asp:TreeNode>
                        <asp:TreeNode ImageUrl="images/WorldCat6.png" SelectAction="None"></asp:TreeNode>
                        <asp:TreeNode Text="After the search results are displayed, you can refine these searchers by format, author, year, language, content, audience and topic." Value="After the search results are displayed, you can refine these searchers by format, author, year, language, content, audience and topic." SelectAction="None"></asp:TreeNode>
                        <asp:TreeNode ImageUrl="images/WorldCat7.png" SelectAction="None"></asp:TreeNode>
                        <asp:TreeNode Text="Information of the readable content is shown once clicked, such as libraries offering this book as well as book details." Value="Information of the readable content is shown was clicked, such as libraries offering this book as well as book details." SelectAction="None"></asp:TreeNode>
                        <asp:TreeNode ImageUrl="images/WorldCat8.png" SelectAction="None"></asp:TreeNode>
                    </asp:TreeNode>
                </asp:TreeNode>
            </Nodes>
        </asp:TreeView>
</p>

</asp:Content>