﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Data.SqlServerCe;
using System.Linq;
using System.Web;
using System.Web.UI;
using WebApplication1.Models;
using System.IO;
using System.Configuration;
using System.Security.Cryptography;
using System.Data;
using System.Text;

namespace WebApplication1.Account
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnLogin_Click(object sender, EventArgs e) {      
            //Open a SQL Connection
            SqlCeConnection con = new SqlCeConnection("Data Source=|DataDirectory|\\Database1.sdf; Persist Security Info=False");
            con.Open();

            // Query to see if the username and password is correct
            SqlCeCommand cmd = new SqlCeCommand("SELECT COUNT(*) FROM Users WHERE Username = @username AND Password = @password", con);
            cmd.Parameters.AddWithValue("@username", UserName.Text);
            cmd.Parameters.AddWithValue("@password", Encryption.Encrypt(Password.Text));
            int a = (int)cmd.ExecuteScalar();
           

            if (a == -1 || a == 0) {
                //Username or password does not exist, set lblStatus to Incorrert details
                lblLoginFail.Visible = true;
                lblLoginSuccess.Visible = false;
                                
            } else {        
                // Make a Cookie and input the Username
                HttpCookie CookyName = new HttpCookie("Username");
                CookyName.Value = UserName.Text;
                Response.Cookies.Add(CookyName);
                
                // Successful login, show message, disable login fields
                lblLoginSuccess.Visible = true;
                lblLoginFail.Visible = false;
                UserName.Enabled = false;
                Password.Enabled = false;
                btnLogin.Enabled = false;

            }

            // Close the connection
            con.Close();

        }

     
    }
}