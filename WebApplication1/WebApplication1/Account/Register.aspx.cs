﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Data.SqlServerCe;
using System.Linq;
using System.Web;
using System.Web.UI;
using WebApplication1.Models;
using System.IO;
using System.Configuration;
using System.Security.Cryptography;
using System.Data;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace WebApplication1.Account
{
    public partial class Register : Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

      
        protected void CreateAccount_Click(object sender, EventArgs e) {
            // check there is something in each field
            string testPostcode = tbPostcode.Text;

            string testUsername =  UserName.Text;
            string password =  Password.Text;
            string testEmail=  tbEmail.Text;
            double testAge =  Convert.ToDouble(Age.SelectedValue);
            string testCountry =  tbCountry.Text;
            string testState =  tbState.Text;
            string validcode = tbValidate.Text;

            if (testUsername == null || testUsername == "" || testUsername.Length > 30) {
                lblError.Text = "Username is missing or too long!";
            } else if (PasswordCheck.ComplexityCheck(password) == false || password.Length > 30) {
                lblError.Text = "Password does not meet complexity requirements or is over 30 characters!";
            }else if(testEmail == null || testEmail == "" || testUsername.Length > 50) {
                lblError.Text = "email is missing or too long!";
            }else if(testAge < 15) {
                lblError.Text = "Age is missing or your not Old enough to have an account!";
            }else if(testCountry == null || testCountry == "" || testCountry == "Select Country") {
                lblError.Text = "Country is missing!";
            } else if (testState == null || testState == "") {
                lblError.Text = "State is missing!";
            } else if (validcode != "1234") {
                lblError.Text = "Valid is incorrect!";
            } else {
                SqlCeConnection con = new SqlCeConnection("Data Source=|DataDirectory|\\Database1.sdf; Persist Security Info=False");
                // check for duplicate username first
                con.Open();
                SqlCeCommand cmd = new SqlCeCommand("SELECT * FROM Users WHERE Username = @u", con);
                cmd.Parameters.AddWithValue("@u", UserName.Text);
                SqlCeDataAdapter da2 = new SqlCeDataAdapter(cmd);
                DataSet ds2 = new DataSet();
                da2.Fill(ds2);

                // create an empty ID
                Double NewUserID = 0;

                if (ds2.Tables[0].Rows.Count == 0) {
                    //Username entered does not exist, create a new UID

                    SqlCeCommand cmd2 = new SqlCeCommand("SELECT * FROM Users order by UID desc ", con);
                    SqlCeDataAdapter da = new SqlCeDataAdapter(cmd2);
                    DataSet ds = new DataSet();
                    da.Fill(ds);


                    double IDNumber = Convert.ToDouble(ds.Tables[0].Rows[0]["UID"]);
                    NewUserID = IDNumber + 7;// just need to get value of the highest ID number and inciment then pass to the doube NewUserID variable an return it allong with adding a new row

                    // add the account, make sure the password is entered as a hash value
                    SqlCeCommand cmd3 = new SqlCeCommand("INSERT INTO Users(Username,Password,Email,Postcode,State,Country,Age,UID) VALUES(@u,@pa,@e,@po,@s,@c,@a,@UID)", con);
                    cmd3.Parameters.AddWithValue("@po", tbPostcode.Text);
                    cmd3.Parameters.AddWithValue("@u", UserName.Text);
                    cmd3.Parameters.AddWithValue("@pa", Encryption.Encrypt(Password.Text));
                    cmd3.Parameters.AddWithValue("@e", tbEmail.Text);
                    cmd3.Parameters.AddWithValue("@a", Age.SelectedValue);
                    cmd3.Parameters.AddWithValue("@c", tbCountry.Text);
                    cmd3.Parameters.AddWithValue("@s", tbState.Text);
                    cmd3.Parameters.AddWithValue("@UID", NewUserID);
                    cmd3.ExecuteNonQuery();
                    

                    // need to insert a Message, Book and block username info in the tables!!!
                    SqlCeCommand cmd4 = new SqlCeCommand("INSERT INTO Block(Username) VALUES(@u)", con);
                    cmd4.Parameters.AddWithValue("@u", UserName.Text);
                    cmd4.ExecuteNonQuery();
                    SqlCeCommand cmd5 = new SqlCeCommand("INSERT INTO Message(Username) VALUES(@u)", con);
                    cmd5.Parameters.AddWithValue("@u", UserName.Text);
                    cmd5.ExecuteNonQuery();
                    

                    //Close the connection
                    con.Close();


                    lblSuccess.Text = "Your account has been created!, please login";
                    lblError.Visible = false;
                } else {
                    lblError.Visible = true;
                    lblError.Text = "A user with the Same name already exists!";
                }
            }
        }
        
        protected void btnSendValidCode_Click(object sender, EventArgs e) {
            // Details to send the Email for verification
            MailAddress to = new MailAddress(tbEmail.Text);
            MailAddress from = new MailAddress("MooseKnuckles90@gmail.com");
            MailMessage mail = new MailMessage(from, to);
            mail.Subject = "Verify Code";
            mail.Body = "Verify Code is 1234"; // for testing purposes, set to 1234. Will be changed in a Future release to give a rendom number

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;

            // Use Moose Knuckles Email cridentails to send the email
            smtp.Credentials = new NetworkCredential(
                "MooseKnuckles90@gmail.com", "Knuckles90");
            smtp.EnableSsl = true;
            smtp.Send(mail);
        }
    
    }

}