﻿<%@ Page Title="Search" Language="C#"  AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Search.aspx.cs" Inherits="WebApplication1.Search" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <hgroup class="title">
        <h1><%: Title %> TextShare</h1>       
        
    </hgroup>
    
        <p><asp:Label ID="lblStatus" runat="server" Font-Bold="True" ForeColor="Red" Text="Your are not logged in or there was an error!" Visible="False"></asp:Label></p>
        
    <asp:Label ID="Label1" runat="server" Text="Search for either user or books details"></asp:Label>
    <hgroup>        
            </hgroup>
            <asp:Button ID="Button1" runat="server" Text="User details" OnClick="Button1_Click1" />
            <asp:Button ID="Button2" runat="server" Text="Book details" OnClick="Button2_Click" />
    
            <hgroup>
    </hgroup>
    
    <p>
        <asp:RadioButtonList ID="RadioButtonList1" runat="server" CellPadding="8" RepeatDirection="Horizontal">
            <asp:ListItem>Postcode</asp:ListItem>
            <asp:ListItem>Country</asp:ListItem>
            <asp:ListItem>State</asp:ListItem>
            <asp:ListItem>Username</asp:ListItem>
        </asp:RadioButtonList>
    </p>

    
    <p>
        <asp:RadioButtonList ID="RadioButtonList2" runat="server" Visible="False" CellPadding="8" RepeatDirection="Horizontal">
            <asp:ListItem>Book Name</asp:ListItem>
            <asp:ListItem>Subject</asp:ListItem>
            <asp:ListItem>Author</asp:ListItem>
            <asp:ListItem>Version Number</asp:ListItem>
        </asp:RadioButtonList>
    </p>
    <p>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="RadioButtonList1" CssClass="text-danger" Display="Dynamic" ErrorMessage="Please choose search option from User details"></asp:RequiredFieldValidator>
    </p>
    <p>
        <asp:TextBox ID="TextBox2" runat="server" Visible="False"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="RadioButtonList2" CssClass="text-danger" Display="Dynamic" Enabled="False" ErrorMessage="Please choose search option from Book details"></asp:RequiredFieldValidator>
    </p>
    <p>
        <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Search" />
    </p>
    <p>
        &nbsp;</p>
    <asp:GridView ID="GridView1" runat="server">
    </asp:GridView>
    <p>
        <asp:Button ID="Button4" runat="server" OnClick="Button4_Click" Text="Export" />
    </p>
    <p>
        <asp:Button ID="Button5" runat="server" OnClick="Button5_Click" Text="Message book owner"  />
    </p>
    <p>
        <asp:Label ID="Label2" runat="server" Text="Users Name:" Visible="False"></asp:Label>
        <asp:TextBox ID="TextBox3" runat="server" Visible="False"></asp:TextBox>
    </p>
    <p>
        <asp:Label ID="Label3" runat="server" Text="In Reguards to TextBook:" Visible="False"></asp:Label>
        <asp:TextBox ID="TextBox4" runat="server" Visible="False"></asp:TextBox>
        <asp:Label ID="lblError" runat="server" Text="Error: You are either blocked, Over the word limit or have the character &amp; in your message!" Visible="False"></asp:Label>
    </p>
    <p>
        <asp:Button ID="Button6" runat="server" OnClick="Button6_Click" Text="Submit book request" Visible="False" />
    </p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>

</asp:Content>
<%--  --%>