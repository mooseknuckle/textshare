﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Messages.aspx.cs" Inherits="WebApplication1.Messages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
            <asp:Label ID="lblStatus" runat="server" Font-Bold="True" Font-Size="Larger" ForeColor="Red" Text="Please login to use this Feature" Visible="False"></asp:Label>
</p>
    <p>
            <asp:Label ID="lblNOMSG" runat="server" Font-Bold="True" Font-Size="Larger" ForeColor="#FF9933" Text="You have no Messages" Visible="False"></asp:Label>
            <br />
</p>
<div>
            <asp:Label ID="lblM1" runat="server" BorderStyle="Groove" Text="Message 1" Width="509px" Visible="False"></asp:Label>
            <br />
            <asp:TextBox ID="tbM1" runat="server" BorderStyle="Dotted" Height="63px" ReadOnly="True" TextMode="MultiLine" Width="507px" Visible="False"></asp:TextBox>
        
            </div>
    <div class="form-group">
            <asp:Label ID="lblM2" runat="server" BorderStyle="Groove" Text="Message 2" Width="509px" Visible="False"></asp:Label>
            <br />
            <asp:TextBox ID="tbM2" runat="server" BorderStyle="Dotted" Height="63px" ReadOnly="True" TextMode="MultiLine" Width="507px" Visible="False"></asp:TextBox>

            </div>
    <div>
            <asp:Label ID="lblM3" runat="server" BorderStyle="Groove" Text="Message 3" Width="509px" Visible="False"></asp:Label>
            <br />
            <asp:TextBox ID="tbM3" runat="server" BorderStyle="Dotted" Height="63px" ReadOnly="True" TextMode="MultiLine" Width="507px" Visible="False"></asp:TextBox>

            </div>
    <div>
            <asp:Label ID="lblM4" runat="server" BorderStyle="Groove" Text="Message 4" Width="509px" Visible="False"></asp:Label>
            <br />
            <asp:TextBox ID="tbM4" runat="server" BorderStyle="Dotted" Height="63px" ReadOnly="True" TextMode="MultiLine" Width="507px" Visible="False"></asp:TextBox>
</div>
    <div>
            <asp:Label ID="lblM5" runat="server" BorderStyle="Groove" Text="Message 5" Width="509px" Visible="False"></asp:Label>
            <br />
            <asp:TextBox ID="tbM5" runat="server" BorderStyle="Dotted" Height="63px" ReadOnly="True" TextMode="MultiLine" Width="507px" Visible="False"></asp:TextBox>
            <br />
</div>
    <div>
            <asp:Button ID="btnDeleteMessages" runat="server" BorderStyle="Groove" Height="33px" OnClick="btnDeleteMessages_Click" Text="Delete all Messages" Width="512px" Visible="False" />
            <br />
            <br />
            <asp:Button ID="btnMessageRefresh" runat="server" Text="Refresh my Messages!"  BorderStyle="Groove" Height="37px" Width="276px" Visible="False" OnClick="btnMessageRefresh_Click" />
            &nbsp;<asp:Button ID="btnShowSendMessage" runat="server" Text="Send a Message!"  BorderStyle="Groove" Height="37px" Width="276px" Visible="False" OnClick="btnShowSendMessage_Click" />
            <br />
            <asp:Button ID="btnMessageExport" runat="server" Text="Export my Messages!"  BorderStyle="Groove" Height="37px" Width="276px" Visible="False" OnClick="btnMessageExport_Click" />
            &nbsp;<asp:Button ID="btnShowBlockedUsersList" runat="server" Text="Show blocked Users!"  BorderStyle="Groove" Height="37px" Width="276px" Visible="False" OnClick="btnShowBlockedUsersList_Click" />
            <br />
            <br />
            </div>
            <div >
                <asp:Label ID="lblsendmsg" runat="server" Text="Send a message to:" Visible="False"></asp:Label>
                <asp:TextBox ID="tbContactUsername" runat="server" Visible="False"></asp:TextBox>
                <br />
                <asp:Label ID="lblentermsg" runat="server" Text="Enter Message: " Visible="False"></asp:Label>
                <asp:TextBox ID="tbUserMessage" runat="server" Width="426px" Visible="False"></asp:TextBox>
                <br />
                <asp:Button ID="btnSendMessage" runat="server" BorderStyle="Ridge" Height="34px" OnClick="btnSendMessage_Click" Text="Send message" Width="273px" Visible="False" />
                &nbsp;<asp:Label ID="lblError" runat="server" Text="Error: You are either blocked, Over the word limit or have the character &amp; in your message!" Visible="False"></asp:Label>
                <br />

            </div>
    <div>
            <asp:Label ID="lblblkusertitle" runat="server" Text="List of blocked users:" Font-Bold="True" Visible="False"></asp:Label>
            
            <br />
            <asp:Label ID="lblB1" runat="server" Visible="False"></asp:Label>
            <br />
            <asp:Label ID="lblB2" runat="server" Visible="False"></asp:Label>
            <br />
            <asp:Label ID="lblB3" runat="server" Visible="False"></asp:Label>
            <br />
            <asp:Label ID="lblB4" runat="server" Visible="False"></asp:Label>
            <br />
            <asp:Label ID="lblB5" runat="server" Visible="False"></asp:Label>
            <br />
            <asp:Button ID="btnShowBlockUser" runat="server" Text="Block a User!"  BorderStyle="Groove" Height="37px" Width="184px" Visible="False" OnClick="btnShowBlockUser_Click" />
            &nbsp;<asp:Button ID="btnRefreshBlockUsers" runat="server" BorderStyle="Groove" Height="37px" Text="Refresh blocked users" Width="184px" Visible="False" OnClick="btnRefreshBlockUsers_Click" />
            &nbsp;<asp:Button ID="btnDeleteBlockedUsers" runat="server" BorderStyle="Groove" Height="37px" OnClick="btnDeleteBlockedUsers_Click" Text="Delete all blocked Users" Width="184px" Visible="False" />
            <br />
            <br />
            </div>
    <div class="form-group">
            <asp:Label ID="lblnameofuser" runat="server" Text="I want to Block this User:" Visible="False"></asp:Label>
            <asp:TextBox ID="tbBlockUserName" runat="server" Visible="False"></asp:TextBox>
            <br />
            <asp:Button ID="btnBlockUser" runat="server" Text="Block this User!"  BorderStyle="Groove" Height="37px" Width="276px" Visible="False" OnClick="btnBlockUser_Click" />
            </div>
    <div>
            <asp:GridView ID="GVUserMessages" runat="server" Visible="False">
            </asp:GridView>
            </div>
    
</asp:Content>
