﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1 {
    public partial class MyAccount : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            
            try {

                // Get the Username from the Cookies
                string CookyUsername = Request.Cookies["Username"].Value;

                DataSet ds = new DataSet();
                ds = SQLSearch.Username("SELECT * FROM Books WHERE Owner = @u", CookyUsername);

                int count;
                count = ds.Tables[0].Rows.Count;
 

                lblDisplayBooksnumber.Text = Convert.ToString(count);
                lblDisplayUsername.Text = CookyUsername;
                
                DataSet ds2 = new DataSet();
                ds2 = SQLSearch.Username("SELECT Email FROM Users WHERE Username = @u", CookyUsername);

                string email = Convert.ToString(ds2.Tables[0].Rows[0]["Email"]);
                lblDisplayEmail.Text = email;

                lblStatus.Visible = false;

                showUserInfo();
                
            } catch {
                //error message
                lblStatus.Visible = true;
            }
        }

        public void showUserInfo() {
            lblbooksnumber.Visible = true;
            lblusername.Visible = true;
            lblemail.Visible = true;
            lblDisplayEmail.Visible = true;
            lblDisplayBooksnumber.Visible = true;
            lblDisplayUsername.Visible = true;
            lbl1.Visible = true;
            lbl2.Visible = true;
            lbl3.Visible = true;
            lblpassowrdmesg.Visible = true;
            tbCurrentPassword.Visible = true;
            tbNewPassword.Visible = true;
            tbNewPassword2.Visible = true;
            btnChangePassword.Visible = true;
        }

        public void hideUserInfo() {
            lblbooksnumber.Visible = false;
            lblusername.Visible = false;
            lblemail.Visible = false;
            lblDisplayEmail.Visible = false;
            lblDisplayBooksnumber.Visible = false;
            lblDisplayUsername.Visible = false;
            lbl1.Visible = false;
            lbl2.Visible = false;
            lbl3.Visible = false;
            lblpassowrdmesg.Visible = false;
            tbCurrentPassword.Visible = false;
            tbNewPassword.Visible = false;
            tbNewPassword2.Visible = false;
            btnChangePassword.Visible = false;
        }

        protected void btnChangePassword_Click(object sender, EventArgs e) {
            try{
                string CurrentPassword = tbCurrentPassword.Text;
                string NewPassword = tbNewPassword.Text;
                string NewPassword2 = tbNewPassword2.Text;

                // Get the Username from the Cookies
                string CookyUsername = Request.Cookies["Username"].Value;

                //Open a SQL Connection
                SqlCeConnection con = new SqlCeConnection("Data Source=|DataDirectory|\\Database1.sdf; Persist Security Info=False");
                con.Open();

                // Query to see if the username and password is correct
                SqlCeCommand cmd = new SqlCeCommand("SELECT COUNT(*) FROM Users WHERE Username = @username AND Password = @password", con);
                cmd.Parameters.AddWithValue("@username", CookyUsername);
                cmd.Parameters.AddWithValue("@password", Encryption.Encrypt(CurrentPassword));
                int a = (int)cmd.ExecuteScalar();
           

                if (a == -1 || a == 0) {
                    //Username or password does not exist
                    lblError.Text = "Current Passeword is incorrect or not logged in";  
                } else if (NewPassword != NewPassword2) {
                    lblError.Text = "Confirm Password does not match the New Password Text!";
                } else if (PasswordCheck.ComplexityCheck(NewPassword) == false) {
                    lblError.Text = "Password does not meet complexity requirements!";
                } else {
                    // change passwords
                    SqlCeCommand cmd3 = new SqlCeCommand("UPDATE Users set Password=@p WHERE Username = @u", con);
                    cmd3.Parameters.AddWithValue("@u", CookyUsername);
                    cmd3.Parameters.AddWithValue("@p",  Encryption.Encrypt(NewPassword));
                    cmd3.ExecuteNonQuery();

                }
                con.Close();

                lblStatus.Visible = false;
                lblSuccess.Visible = true;

            } catch {
                lblStatus.Visible = true;
                lblSuccess.Visible = true;
            }

        }

       

    }
}