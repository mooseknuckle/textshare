﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="WebApplication1.About1" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <hgroup class="title">
        <h1><%: Title %> TextShare</h1>       
    </hgroup>
    <p>&nbsp;</p>

    <article>
        <p>TextShare allows you to connect and share textbooks to other students. Connected by our systems, you will able to view students around your area, message them as well as view status on what books you loan out and borrow. TextShare also allows you to save and export these details to view whenever, forever.</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
                
    </article>

    <p><b><u>Additional Information</u></b></p>

    <p><b>Updated: </b>25/09/2015</p>
        
    <p><b>Current Version: </b>1.1</p>

    
    <p>&nbsp;</p>

</asp:Content>