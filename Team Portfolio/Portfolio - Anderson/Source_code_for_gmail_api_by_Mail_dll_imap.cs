﻿using Limilabs.Client.IMAP;
using Limilabs.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1 {
    class Program {
        static void Main(string[] args) {
            using (Imap imap = new Imap()) {

         
                imap.ConnectSSL("imap.gmail.com");
                imap.Login("MooseKnuckles90@gmail.com", "Knuckles90");

                imap.SelectInbox();

                List<long> uids = imap.SearchFlag(Flag.Unseen);

                foreach (long uid in uids) {
                    string eml;
                     eml = Convert.ToString (imap.GetMessageByUID(uid));
                    IMail email = new MailBuilder().CreateFromEml(eml);

                    Console.WriteLine(email.Subject);
                    Console.WriteLine(email.TextDataString);
                }

                imap.Close(true);
            }
            Console.ReadLine();
        }
    }
}
