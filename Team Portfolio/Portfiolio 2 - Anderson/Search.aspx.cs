﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;



namespace WebApplication1 {
    public partial class Search : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {

        }



        protected void Button1_Click1(object sender, EventArgs e) {

            RadioButtonList1.Visible = true;
            RadioButtonList2.Visible = false;
            TextBox1.Visible = true;
            TextBox2.Visible = false;
            RequiredFieldValidator1.Enabled = true;
            RequiredFieldValidator2.Enabled = false;
            RadioButtonList2.ClearSelection();
        }

        protected void Button2_Click(object sender, EventArgs e) {

            RadioButtonList1.Visible = false;
            RadioButtonList2.Visible = true;
            TextBox1.Visible = false;
            TextBox2.Visible = true;
            RequiredFieldValidator1.Enabled = false;
            RequiredFieldValidator2.Enabled = true;
            RadioButtonList1.ClearSelection();
        }

        protected void Button3_Click(object sender, EventArgs e) {

            //RadioButtonList1.SelectedIndex = 2;

            string userSearchTextBoxInput = null;
            //Open A SQL connection and do a query obtaining the book
            SqlCeConnection con = new SqlCeConnection("Data Source=|DataDirectory|\\Database1.sdf; Persist Security Info=False");
            con.Open();

            //Checks if User details was selected
            if (TextBox1.Visible == true) {

                // Set Variables
                userSearchTextBoxInput = TextBox1.Text;
                DataSet ds = new DataSet();

                if (RadioButtonList1.SelectedIndex == 0) { //Checks if postcode is selected for search option
                    ds = SQLSearch.BasicSearch("SELECT * FROM Books WHERE Postcode = @value", userSearchTextBoxInput);

                } else if (RadioButtonList1.SelectedIndex == 1) { //Checks if country is selected for search option
                    ds = SQLSearch.BasicSearch("SELECT * FROM Books WHERE Country = @value", userSearchTextBoxInput);

                } else if (RadioButtonList1.SelectedIndex == 2) { //Checks if state is selected for search option
                    ds = SQLSearch.BasicSearch("SELECT * FROM Books WHERE State = @value", userSearchTextBoxInput);

                } else if (RadioButtonList1.SelectedIndex == 3) { //Checks if username is selected for search option
                    ds = SQLSearch.BasicSearch("SELECT Username,Postcode,Country FROM Users WHERE Username = @value", userSearchTextBoxInput);
                    
                }

                // Copy the Data to the Grid View and Bind it
                GridView1.DataSource = ds;
                GridView1.DataBind();

            }



            //Checks if Book details was selected
            if (TextBox2.Visible == true) {

                // Set Variables
                userSearchTextBoxInput = TextBox2.Text;
                DataSet ds = new DataSet();

                if (RadioButtonList2.SelectedIndex == 0) { //Checks if Book name is selected for search option
                    ds = SQLSearch.BasicSearch("SELECT * FROM Books WHERE Title = @value", userSearchTextBoxInput);

                } else if (RadioButtonList2.SelectedIndex == 1) { //Checks if Subject is selected for search option
                    userSearchTextBoxInput = TextBox2.Text;
                    ds = SQLSearch.BasicSearch("SELECT * FROM Books WHERE Subject = @value", userSearchTextBoxInput);

                } else if (RadioButtonList2.SelectedIndex == 2) { //Checks if Author is selected for search option
                    userSearchTextBoxInput = TextBox2.Text;
                    ds = SQLSearch.BasicSearch("SELECT * FROM Books WHERE Author = @value", userSearchTextBoxInput);

                } else if (RadioButtonList2.SelectedIndex == 3) { //Checks if Version number is selected for search option
                    userSearchTextBoxInput = TextBox2.Text;
                    ds = SQLSearch.BasicSearch("SELECT * FROM Books WHERE Version = @value", userSearchTextBoxInput);

                }
                // Copy the Data to the Grid View and Bind it
                GridView1.DataSource = ds;
                GridView1.DataBind();

            }


        }

        protected void Button4_Click(object sender, EventArgs e) {
            //Build the Text file data.
            string txt = string.Empty;

            GridViewRow header = GridView1.HeaderRow;

            foreach (TableCell cell in header.Cells) {
                //Add the Header row for Text file.
                txt += cell.Text + "\t\t";
            }

            //Add new line.
            txt += "\r\n";

            foreach (GridViewRow row in GridView1.Rows) {
                foreach (TableCell cell in row.Cells) {
                    //Add the Data rows.
                    txt += cell.Text + "\t\t";
                }

                //Add new line.
                txt += "\r\n";
            }

            //Download the Text file.
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.txt");
            Response.Charset = "";
            Response.ContentType = "application/text";
            Response.Output.Write(txt);
            Response.Flush();
            Response.End();
        }

        protected void Button6_Click(object sender, EventArgs e) {
            try {
                // Get the Username from the Cookies
                string CookyUsername = Request.Cookies["Username"].Value;

                //Open A SQL connection and do a query obtaining the book with the logged in username only
                SqlCeConnection con = new SqlCeConnection("Data Source=|DataDirectory|\\Database1.sdf; Persist Security Info=False");
                con.Open();

                // Get the Contact Persons Username
                string ContactsName = Convert.ToString(TextBox3.Text);
                bool send = true;

                //Open A SQL connection and do a query obtaining the list of blocked users with the logged in username
                SqlCeCommand cmd2 = new SqlCeCommand("SELECT * FROM Block WHERE Username = @u", con);
                cmd2.Parameters.AddWithValue("@u", ContactsName);
                SqlCeDataAdapter da2 = new SqlCeDataAdapter(cmd2);
                DataSet ds2 = new DataSet();
                da2.Fill(ds2);

                // convert the results into strings for comparision
                string b1 = Convert.ToString(ds2.Tables[0].Rows[0]["B1"]);
                string b2 = Convert.ToString(ds2.Tables[0].Rows[0]["B2"]);
                string b3 = Convert.ToString(ds2.Tables[0].Rows[0]["B3"]);
                string b4 = Convert.ToString(ds2.Tables[0].Rows[0]["B4"]);
                string b5 = Convert.ToString(ds2.Tables[0].Rows[0]["B5"]);

                if (CookyUsername == b1) {
                    // If the Username is in the Contacts Block list, then set send to false
                    send = false;
                } else if (CookyUsername == b2) {
                    // If the Username is in the Contacts Block list, then set send to false
                    send = false;
                } else if (CookyUsername == b3) {
                    // If the Username is in the Contacts Block list, then set send to false
                    send = false;
                } else if (CookyUsername == b4) {
                    // If the Username is in the Contacts Block list, then set send to false
                    send = false;
                } else if (CookyUsername == b5) {
                    // If the Username is in the Contacts Block list, then set send to false
                    send = false;
                }
                Regex regex = new Regex("[&]");
                Match match = regex.Match(TextBox4.Text);
                if (match.Success) {
                    // User included a & symbol in the message, this will break the code later, so dont send the message if this symbol exists
                    send = false;
                    lblError.Visible = true;
                } else if (TextBox4.Text.Length > 199) {
                    // Message is too long
                    send = false;
                    lblError.Visible = true;
                }
                // build the message
                string message = " " + CookyUsername + "& Contacted you in reguards to: " + TextBox4.Text;

                // Send the message if send bool equals true
                if (send) {
                    SqlCeCommand cmd4 = new SqlCeCommand("SELECT * FROM Message WHERE Username = @u", con);
                    cmd4.Parameters.AddWithValue("@u", ContactsName);
                    SqlCeDataAdapter da4 = new SqlCeDataAdapter(cmd4);
                    DataSet ds4 = new DataSet();
                    da4.Fill(ds4);

                    // convert the results into strings for comparision
                    string m1 = Convert.ToString(ds4.Tables[0].Rows[0]["M1"]);
                    string m2 = Convert.ToString(ds4.Tables[0].Rows[0]["M2"]);
                    string m3 = Convert.ToString(ds4.Tables[0].Rows[0]["M3"]);
                    string m4 = Convert.ToString(ds4.Tables[0].Rows[0]["M4"]);
                    string m5 = Convert.ToString(ds4.Tables[0].Rows[0]["M5"]);


                    if (m1 == null || m1 == "") {
                        // send message if free space
                        SqlCeCommand cmd3 = new SqlCeCommand("UPDATE Message set M1=@m WHERE Username = @u", con);
                        cmd3.Parameters.AddWithValue("@u", ContactsName);
                        cmd3.Parameters.AddWithValue("@m", message);
                        cmd3.ExecuteNonQuery();
                    } else if (m2 == null || m2 == "") {
                        // send message if free space
                        SqlCeCommand cmd3 = new SqlCeCommand("UPDATE Message set M2=@m WHERE Username = @u", con);
                        cmd3.Parameters.AddWithValue("@u", ContactsName);
                        cmd3.Parameters.AddWithValue("@m", message);
                        cmd3.ExecuteNonQuery();
                    } else if (m3 == null || m3 == "") {
                        // send message if free space
                        SqlCeCommand cmd3 = new SqlCeCommand("UPDATE Message set M3=@m WHERE Username = @u", con);
                        cmd3.Parameters.AddWithValue("@u", ContactsName);
                        cmd3.Parameters.AddWithValue("@m", message);
                        cmd3.ExecuteNonQuery();
                    } else if (m4 == null || m4 == "") {
                        // send message if free space
                        SqlCeCommand cmd3 = new SqlCeCommand("UPDATE Message set M4=@m WHERE Username = @u", con);
                        cmd3.Parameters.AddWithValue("@u", ContactsName);
                        cmd3.Parameters.AddWithValue("@m", message);
                        cmd3.ExecuteNonQuery();
                    } else if (m5 == null || m5 == "") {
                        // send message if free space
                        SqlCeCommand cmd3 = new SqlCeCommand("UPDATE Message set M5=@m WHERE Username = @u", con);
                        cmd3.Parameters.AddWithValue("@u", ContactsName);
                        cmd3.Parameters.AddWithValue("@m", message);
                        cmd3.ExecuteNonQuery();
                    } // if it get past here then there is no room and the message is not sent

                    // Close the SQL Connection
                    con.Close();

                    //if logged in remove the not logged in message
                    lblStatus.Visible = false;
                    lblError.Visible = false;

                }

            } catch {
                //set login error
                lblStatus.Visible = true;
            }

        }

        protected void Button5_Click(object sender, EventArgs e) {
            Label2.Visible = true;
            TextBox3.Visible = true;
            Label3.Visible = true;
            TextBox4.Visible = true;
            Button6.Visible = true;
        }

    }
}