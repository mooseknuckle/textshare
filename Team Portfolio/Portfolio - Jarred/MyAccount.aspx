﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MyAccount.aspx.cs" Inherits="WebApplication1.MyAccount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        <asp:Label ID="lblStatus" runat="server" Text="You are not logged in!" Visible="False"></asp:Label>
        <br />
    </p>
    <p>
        <asp:Label ID="lblusername" runat="server" Text="Username:" Visible="False"></asp:Label>
        <asp:Label ID="lblDisplayUsername" runat="server" Visible="False"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblemail" runat="server" Text="Email:" Visible="False"></asp:Label>
        <asp:Label ID="lblDisplayEmail" runat="server" Visible="False"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lblbooksnumber" runat="server" Text="Number of books in Collection: " Visible="False"></asp:Label>
        <asp:Label ID="lblDisplayBooksnumber" runat="server" Visible="False"></asp:Label>
    </p>
    <p>
        &nbsp;</p>
    <p>
        <asp:Label ID="lblpassowrdmesg" runat="server" Font-Bold="True" Text="Change my Password" Visible="False"></asp:Label>
    </p>
    <p>
        <asp:Label ID="lbl1" runat="server" Text="Current Password:" Visible="False"></asp:Label>
    </p>
    <p>
        <asp:TextBox ID="tbCurrentPassword" runat="server" Visible="False" TextMode="Password"></asp:TextBox>
    </p>
    <p>
        &nbsp;</p>
    <p>
        <asp:Label ID="lbl2" runat="server" Text="New Password" Visible="False"></asp:Label>
    </p>
    <p>
        <asp:TextBox ID="tbNewPassword" runat="server" Visible="False" TextMode="Password"></asp:TextBox>
    </p>
    <p>
        <asp:Label ID="lbl3" runat="server" Text="Re-Enter New Password" Visible="False"></asp:Label>
    </p>
    <p>
        <asp:TextBox ID="tbNewPassword2" runat="server" Visible="False" TextMode="Password"></asp:TextBox>
    </p>
    <p>
        <asp:Button ID="btnChangePassword" runat="server" BorderStyle="Groove" OnClick="btnChangePassword_Click" Text="Change my Password" Visible="False" />
&nbsp;</p>
    <p>
        <asp:Label ID="lblError" runat="server" Visible="False"></asp:Label>
        <asp:Label ID="lblSuccess" runat="server" Font-Bold="True" ForeColor="#006600" Text="Your Password has been successfully changed!" Visible="False"></asp:Label>
    </p>
    </asp:Content>
