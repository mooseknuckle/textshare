﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace WebApplication1 {
    public class PasswordCheck {

        /* Function ComplexityCheck
         * 
         * Discription: Checks the String for one Uppercase letter, one lower Case letter and a Digit.
         *              If False is returned the string does not meet compleity requirements.
         * 
         * input: String to be checked
         * output: bool value 
         * 
         */
        public static bool ComplexityCheck(string password) {
            if (password.Any(c => char.IsUpper(c))) {
                if (password.Any(c => char.IsLower(c))) {
                    if (password.Any(c => char.IsDigit(c))) {
                        return true;
                    }
                }
            }
            return false;
            
        }


    }
}