﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1 {
    public partial class UserCatalogue : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            try {

                // Get the Username from the Cookies
                string CookyUsername = Request.Cookies["Username"].Value;

                DataSet ds = new DataSet();
                ds = SQLSearch.Username("SELECT * FROM Books WHERE Owner = @u", CookyUsername);

                // Copy the Data to the Grid View and Bind it
                UserGV.DataSource = ds;
                UserGV.DataBind();

                //if logged in remove the not logged in message
                lblStatus.Visible = false;

                // make the buttons visable
                btnAddBook.Visible = true;
                btnRefreshBooks.Visible = true;
                btnSetBookLoan.Visible = true;
                btnExportMyBooks.Visible = true;

            } catch {
                // set login error
                lblStatus.Visible = true;

            }

        }

        protected void btnRefreshBooks_Click(object sender, EventArgs e) {
            try {

                // Get the Username from the Cookies
                string CookyUsername = Request.Cookies["Username"].Value;

                DataSet ds = new DataSet();
                ds = SQLSearch.Username("SELECT * FROM Books WHERE Owner = @u", CookyUsername);

                // Copy the Data to the Grid View and Bind it
                UserGV.DataSource = ds;
                UserGV.DataBind();

                //if logged in remove the not logged in message
                lblStatus.Visible = false;

            } catch {
                // set login error
                lblStatus.Visible = true;
            }
        }

        protected void btnAddBook_Click(object sender, EventArgs e) {
            // display the fields for adding the book below
            lblTitle.Visible = true;
            lblSubject.Visible = true;
            lblAuthor.Visible = true;
            lblVersion.Visible = true;
            tbNewBookTitle.Visible = true;
            tbNewBookSubject.Visible = true;
            tbNewBookAuthor.Visible = true;
            tbNewBookVersion.Visible = true;
            btnCreateNewBook.Visible = true;
        }

        protected void btnCreateNewBook_Click(object sender, EventArgs e) {
            try {
                // Get the Username from the Cookies
                string CookyUsername = Request.Cookies["Username"].Value;

                //Open A SQL connection and do a query obtaining the all of the Users details using the logged in username
                SqlCeConnection con = new SqlCeConnection("Data Source=|DataDirectory|\\Database1.sdf; Persist Security Info=False");
                con.Open();
                SqlCeCommand cmd = new SqlCeCommand("SELECT * FROM Users WHERE Username = @u", con);
                cmd.Parameters.AddWithValue("@u", CookyUsername);
                SqlCeDataAdapter da = new SqlCeDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                // copy the results into strings
                string username = Convert.ToString(ds.Tables[0].Rows[0]["Username"]);
                string postcode = Convert.ToString(ds.Tables[0].Rows[0]["Postcode"]);
                string state = Convert.ToString(ds.Tables[0].Rows[0]["State"]);
                string country = Convert.ToString(ds.Tables[0].Rows[0]["Country"]);

                // get the entered details and put them into strings
                string title = Convert.ToString(tbNewBookTitle.Text);
                string author = Convert.ToString(tbNewBookAuthor.Text);
                string subject = Convert.ToString(tbNewBookSubject.Text);
                string version = Convert.ToString(tbNewBookVersion.Text);

                // Need to creat a Unique ID, so obtain the largest ID number from the Books Table and then increment by 2
                SqlCeCommand cmd5 = new SqlCeCommand("SELECT * FROM Books ORDER BY ID desc", con);
                SqlCeDataAdapter da5 = new SqlCeDataAdapter(cmd5);
                DataSet ds5 = new DataSet();
                da5.Fill(ds5);

                // convert to Double and incriment by 2
                double newid = Convert.ToDouble(ds5.Tables[0].Rows[0]["ID"]);
                newid = newid + 2;

                //Open A SQL connection and Insert the book with the aquired details
                SqlCeCommand cmd3 = new SqlCeCommand("INSERT INTO Books(Title,Author,Subject,Postcode,State,Country,Version,Owner,ID) VALUES(@t,@a,@su,@po,@st,@c,@v,@o,@id)", con);
                cmd3.Parameters.AddWithValue("@po", postcode);
                cmd3.Parameters.AddWithValue("@t", title);
                cmd3.Parameters.AddWithValue("@a", author);
                cmd3.Parameters.AddWithValue("@su", subject);
                cmd3.Parameters.AddWithValue("@st", state);
                cmd3.Parameters.AddWithValue("@c", country);
                cmd3.Parameters.AddWithValue("@v", version);
                cmd3.Parameters.AddWithValue("@o", username);
                cmd3.Parameters.AddWithValue("@id", newid);
                cmd3.ExecuteNonQuery();

                // Close the SQL connection
                con.Close();

                //if logged in remove the not logged in message
                lblStatus.Visible = false;

            } catch {
                // set login error
                lblStatus.Visible = true;
            }
        }

        protected void btnLoanSetBook_Click(object sender, EventArgs e) {
            try {
                // Get the Username from the Cookies
                string CookyUsername = Request.Cookies["Username"].Value;

                //Open A SQL connection and do a query obtaining the all of the Users details using the logged in username
                SqlCeConnection con = new SqlCeConnection("Data Source=|DataDirectory|\\Database1.sdf; Persist Security Info=False");
                con.Open();

                // get the entered variables
                double bookid = Convert.ToDouble(tbTextBookIDNumber.Text);
                string date = Convert.ToString(tbReturnDate.Text);
                string borrower = Convert.ToString(tbBorrowerUsername.Text);

                // enter the details with the info given
                SqlCeCommand cmd3 = new SqlCeCommand("UPDATE Books set Loaned=@b WHERE Owner = @u AND ID = @id", con);
                cmd3.Parameters.AddWithValue("@u", CookyUsername);
                cmd3.Parameters.AddWithValue("@b", borrower);
                cmd3.Parameters.AddWithValue("@id", bookid);
                cmd3.ExecuteNonQuery();
                SqlCeCommand cmd4 = new SqlCeCommand("UPDATE Books set ReturnDate=@r WHERE Owner = @u AND ID = @id", con);
                cmd4.Parameters.AddWithValue("@u", CookyUsername);
                cmd4.Parameters.AddWithValue("@r", date);
                cmd4.Parameters.AddWithValue("@id", bookid);
                cmd4.ExecuteNonQuery();

                // Close the SQL connection
                con.Close();

                //if logged in remove the not logged in message
                lblStatus.Visible = false;

            } catch {
                // set login error
                lblStatus.Visible = true;
            }
        }

        protected void btnSetBookLoan_Click(object sender, EventArgs e){
            // make the Requested fields Visable
            tbTextBookIDNumber.Visible = true;
            tbBorrowerUsername.Visible = true;
            tbReturnDate.Visible = true;
            lblBookID.Visible = true;
            lblReturndate.Visible = true;
            lblLoanedUsername.Visible = true;
            btnLoanBook.Visible = true;
        }

        protected void btnExportMyBooks_Click(object sender, EventArgs e) {
            //Build the Text file data.
            string txt = string.Empty;

            // Add the Header information
            txt += "\r\n";
            txt += "Title\t\t";
            txt += "Author\t\t";
            txt += "Subject\t\t";
            txt += "Postcode\t";
            txt += "State\t\t";
            txt += "Country\t\t";
            txt += "Version\t\t";
            txt += "Owner\t\t";
            txt += "ID\t\t";
            txt += "Loaned\t\t";
            txt += "ReturnDate\t\t";

            //Add new line.
            txt += "\r\n";

            foreach (GridViewRow row in UserGV.Rows) {
                foreach (TableCell cell in row.Cells) {
                    //Add the Data rows.
                    txt += cell.Text + "\t\t";
                }

                //Add new line.
                txt += "\r\n";
            }

            //Download the Text file.
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.txt");
            Response.Charset = "";
            Response.ContentType = "application/text";
            Response.Output.Write(txt);
            Response.Flush();
            Response.End();        
        }
        
    }
}