﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserCatalogue.aspx.cs" Inherits="WebApplication1.UserCatalogue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Label ID="lblStatus" runat="server" Text="Please login to use this Feature" Font-Bold="True" Font-Size="Larger" ForeColor="Red"></asp:Label>
    <br />
    <asp:GridView ID="UserGV" runat="server">
    </asp:GridView>
    <br />
    <asp:Button ID="btnRefreshBooks" runat="server" OnClick="btnRefreshBooks_Click" Text="Refresh my Book Collection" Visible="False" BorderStyle="Groove" Height="37px" Width="276px" />
&nbsp;<asp:Button ID="btnAddBook" runat="server" OnClick="btnAddBook_Click" Text="Add a Book to my collection" Visible="False" BorderStyle="Groove" Height="37px" Width="276px" />
    <br />
    <asp:Button ID="btnExportMyBooks" runat="server" Text="Export a list of my books" Width="276px" Visible="False" BorderStyle="Groove" Height="37px" OnClick="btnExportMyBooks_Click" />
&nbsp;<asp:Button ID="btnSetBookLoan" runat="server" OnClick="btnSetBookLoan_Click" Text="Set a book as loaned" Width="276px" Visible="False" BorderStyle="Groove" Height="37px" />
    <br />
    <asp:Label ID="lblTitle" runat="server" Text="Title:    " Visible="False"></asp:Label>
    <asp:TextBox ID="tbNewBookTitle" runat="server" Visible="False"></asp:TextBox>
    <br />
    <asp:Label ID="lblSubject" runat="server" Text="Subject:   " Visible="False"></asp:Label>
    <asp:TextBox ID="tbNewBookSubject" runat="server" Visible="False"></asp:TextBox>
    <br />
    <asp:Label ID="lblAuthor" runat="server" Text="Author:  " Visible="False"></asp:Label>
    <asp:TextBox ID="tbNewBookAuthor" runat="server" Visible="False"></asp:TextBox>
    <br />
    <asp:Label ID="lblVersion" runat="server" Text="Version:" Visible="False"></asp:Label>
    <asp:TextBox ID="tbNewBookVersion" runat="server" Visible="False"></asp:TextBox>
    <br />
    <br />
    <asp:Button ID="btnCreateNewBook" runat="server" OnClick="btnCreateNewBook_Click" Text="Add this Book" Width="264px" Visible="False" BorderStyle="Groove" Height="37px" />
    <br />
    <br />
    <asp:Label ID="lblBookID" runat="server" Text="Book ID" Visible="False"></asp:Label>
    <asp:TextBox ID="tbTextBookIDNumber" runat="server" Visible="False"></asp:TextBox>
    <br />
    <asp:Label ID="lblLoanedUsername" runat="server" Text="name of the User" Visible="False"></asp:Label>
    <asp:TextBox ID="tbBorrowerUsername" runat="server" Visible="False"></asp:TextBox>
    <br />
    <asp:Label ID="lblReturndate" runat="server" Text="Return Date" Visible="False"></asp:Label>
    <asp:TextBox ID="tbReturnDate" runat="server" Visible="False"></asp:TextBox>
    <br />
    <br />
    <asp:Button ID="btnLoanBook" runat="server" OnClick="btnLoanSetBook_Click" Text="Set this Book as Loaned!" Visible="False" BorderStyle="Groove" Height="37px" />
    <br />
</asp:Content>
