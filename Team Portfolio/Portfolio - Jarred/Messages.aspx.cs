﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1 {
    public partial class Messages : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {

            try {
                // Get the Username from the Cookies
                string CookyUsername = Request.Cookies["Username"].Value;

                // create a new dataset to hold the Sql Query
                DataSet ds = new DataSet();
                ds = SQLSearch.Username("SELECT M1,M2,M3,M4,M5 FROM Message WHERE Username = @u",CookyUsername);

                // Copy the Data to the textboxes
                string M1 = Convert.ToString(ds.Tables[0].Rows[0]["M1"]);
                string M2 = Convert.ToString(ds.Tables[0].Rows[0]["M2"]);
                string M3 = Convert.ToString(ds.Tables[0].Rows[0]["M3"]);
                string M4 = Convert.ToString(ds.Tables[0].Rows[0]["M4"]);
                string M5 = Convert.ToString(ds.Tables[0].Rows[0]["M5"]);

                // Copy the Data to the textboxes, Split the Users name and the message for display 
                string pattern = "&";
                Regex regex = new Regex("[&]");
                Match match = regex.Match(M1);
                if (match.Success) {
                    string[] word = Regex.Split(M1, pattern);
                    lblM1.Text = word[0];
                    tbM1.Text = word[1];
                } else {
                    lblM1.Text = "";
                    tbM1.Text = "";
                }

                Match match2 = regex.Match(M2);
                if (match2.Success) {
                    string[] word = Regex.Split(M2, pattern);
                    lblM2.Text = word[0];
                    tbM2.Text = word[1];
                } else {
                    lblM2.Text = "";
                    tbM2.Text = "";
                }

                Match match3 = regex.Match(M3);
                if (match3.Success) {
                    string[] word = Regex.Split(M3, pattern);
                    lblM3.Text = word[0];
                    tbM3.Text = word[1];
                } else {
                    lblM3.Text = "";
                    tbM3.Text = "";
                }

                Match match4 = regex.Match(M4);
                if (match4.Success) {
                    string[] word = Regex.Split(M4, pattern);
                    lblM4.Text = word[0];
                    tbM4.Text = word[1];
                } else {
                    lblM4.Text = "";
                    tbM4.Text = "";
                }

                Match match5 = regex.Match(M5);
                if (match5.Success) {
                    string[] word = Regex.Split(M5, pattern);
                    lblM5.Text = word[0];
                    tbM5.Text = word[1];
                } else {
                    lblM5.Text = "";
                    tbM5.Text = "";
                }

                showMessages();


                // create a new dataset to hold the Sql Query
                DataSet ds2 = new DataSet();
                ds2 = SQLSearch.Username("SELECT B1,B2,B3,B4,B5 FROM Block WHERE Username = @u", CookyUsername);

                // Copy the Data to the textboxes
                lblB1.Text = Convert.ToString(ds2.Tables[0].Rows[0]["B1"]);
                lblB2.Text = Convert.ToString(ds2.Tables[0].Rows[0]["B2"]);
                lblB3.Text = Convert.ToString(ds2.Tables[0].Rows[0]["B3"]);
                lblB4.Text = Convert.ToString(ds2.Tables[0].Rows[0]["B4"]);
                lblB5.Text = Convert.ToString(ds2.Tables[0].Rows[0]["B5"]);
                
               
                //if logged in remove the not logged in message
                lblStatus.Visible = false;

                // display main buttons
                showMainButtons();

                //to allow export to work...
                GVUserMessages.DataSource = ds;
                GVUserMessages.DataBind();

            } catch {
                // Error - Display Error Message
                lblStatus.Visible = true;
            }

        }

        protected void btnMessageRefresh_Click(object sender, EventArgs e) {
             try {
                 // Get the Username from the Cookies
                 string CookyUsername = Request.Cookies["Username"].Value;

                 // create a new dataset to hold the Sql Query
                 DataSet ds = new DataSet();
                 ds = SQLSearch.Username("SELECT M1,M2,M3,M4,M5 FROM Message WHERE Username = @u", CookyUsername);

                 // Copy the Data to the textboxes
                 string M1 = Convert.ToString(ds.Tables[0].Rows[0]["M1"]);
                 string M2 = Convert.ToString(ds.Tables[0].Rows[0]["M2"]);
                 string M3 = Convert.ToString(ds.Tables[0].Rows[0]["M3"]);
                 string M4 = Convert.ToString(ds.Tables[0].Rows[0]["M4"]);
                 string M5 = Convert.ToString(ds.Tables[0].Rows[0]["M5"]);

                 // Copy the Data to the textboxes, Split the Users name and the message for display 
                 string pattern = "&";
                 Regex regex = new Regex("[&]");
                 Match match = regex.Match(M1);
                 if (match.Success) {
                     string[] word = Regex.Split(M1, pattern);
                     lblM1.Text = word[0];
                     tbM1.Text = word[1];
                 } else {
                     lblM1.Text = "";
                     tbM1.Text = "";
                 }

                 Match match2 = regex.Match(M2);
                 if (match2.Success) {
                     string[] word = Regex.Split(M2, pattern);
                     lblM2.Text = word[0];
                     tbM2.Text = word[1];
                 } else {
                     lblM2.Text = "";
                     tbM2.Text = "";
                 }

                 Match match3 = regex.Match(M3);
                 if (match3.Success) {
                     string[] word = Regex.Split(M3, pattern);
                     lblM3.Text = word[0];
                     tbM3.Text = word[1];
                 } else {
                     lblM3.Text = "";
                     tbM3.Text = "";
                 }

                 Match match4 = regex.Match(M4);
                 if (match4.Success) {
                     string[] word = Regex.Split(M4, pattern);
                     lblM4.Text = word[0];
                     tbM4.Text = word[1];
                 } else {
                     lblM4.Text = "";
                     tbM4.Text = "";
                 }

                 Match match5 = regex.Match(M5);
                 if (match5.Success) {
                     string[] word = Regex.Split(M5, pattern);
                     lblM5.Text = word[0];
                     tbM5.Text = word[1];
                 } else {
                     lblM5.Text = "";
                     tbM5.Text = "";
                 }

                 showMessages();

                 // No Error - Hide Error Message
                 lblStatus.Visible = false;

             } catch {
                 // Error - Display Error Message
                 lblStatus.Visible = true;
             }

        }

        protected void btnShowSendMessage_Click(object sender, EventArgs e) {
            showMessgestuff();
        }

        protected void btnMessageExport_Click(object sender, EventArgs e) {
            //Build the Text file data.
            string txt = string.Empty;               

            //Add new line.
            txt += "\r\n";

            foreach (GridViewRow row in GVUserMessages.Rows) {
                foreach (TableCell cell in row.Cells) {
                    //Add the Data rows.
                    txt += cell.Text + "\r\n";                   
                }

                //Add new line.
                txt += "\r\n";
            }

            //Download the Text file.
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.txt");
            Response.Charset = "";
            Response.ContentType = "application/text";
            Response.Output.Write(txt);
            Response.Flush();
            Response.End();  
  
        }

        protected void btnShowBlockedUsersList_Click(object sender, EventArgs e) {
            lblblkusertitle.Visible = true;
            btnShowBlockUser.Visible = true;
            btnRefreshBlockUsers.Visible = true;
            btnDeleteBlockedUsers.Visible = true;
            showblocklist();
        }

        protected void btnShowBlockUser_Click(object sender, EventArgs e) {
            lblnameofuser.Visible = true;
            tbBlockUserName.Visible = true;
            btnBlockUser.Visible = true;
        }

        protected void btnSendMessage_Click(object sender, EventArgs e) {
            try {
                // Get the Username from the Cookies
                string CookyUsername = Request.Cookies["Username"].Value;

                // Get the Contact Persons Username
                string ContactsName = Convert.ToString(tbContactUsername.Text);
                bool send = true;

                // Run SQL Query to get list of blocked Users
                DataSet ds = new DataSet();
                ds = SQLSearch.Username("SELECT * FROM Block WHERE Username = @u", ContactsName);

                // convert the results into strings for comparision
                string b1 = Convert.ToString(ds.Tables[0].Rows[0]["B1"]);
                string b2 = Convert.ToString(ds.Tables[0].Rows[0]["B2"]);
                string b3 = Convert.ToString(ds.Tables[0].Rows[0]["B3"]);
                string b4 = Convert.ToString(ds.Tables[0].Rows[0]["B4"]);
                string b5 = Convert.ToString(ds.Tables[0].Rows[0]["B5"]);

                if (CookyUsername == b1) {
                    // If the Username is in the Contacts Block list, then set send to false
                    send = false;
                    lblError.Visible = true;
                } else if (CookyUsername == b2) {
                    // If the Username is in the Contacts Block list, then set send to false
                    send = false;
                    lblError.Visible = true;
                } else if (CookyUsername == b3) {
                    // If the Username is in the Contacts Block list, then set send to false
                    send = false;
                    lblError.Visible = true;
                } else if (CookyUsername == b4) {
                    // If the Username is in the Contacts Block list, then set send to false
                    send = false;
                    lblError.Visible = true;
                } else if (CookyUsername == b5) {
                    // If the Username is in the Contacts Block list, then set send to false
                    send = false;
                    lblError.Visible = true;
                }

                Regex regex = new Regex("[&]");
                Match match = regex.Match(tbUserMessage.Text);
                if (match.Success) {
                    // User included a & symbol in the message, this will break the code later, so dont send the message if this symbol exists
                    send = false;
                    lblError.Visible = true;
                }else if (tbUserMessage.Text.Length > 199){
                    // Message is too long
                    send = false;
                    lblError.Visible = true;
                }

                // build the message
                string message = " " + CookyUsername + "& " + tbUserMessage.Text;

                //Open A SQL connection
                SqlCeConnection con = new SqlCeConnection("Data Source=|DataDirectory|\\Database1.sdf; Persist Security Info=False");
                con.Open();

                // Send the message if send bool equals true
                if (send) {
                    SqlCeCommand cmd4 = new SqlCeCommand("SELECT * FROM Message WHERE Username = @u", con);
                    cmd4.Parameters.AddWithValue("@u", ContactsName);
                    SqlCeDataAdapter da4 = new SqlCeDataAdapter(cmd4);
                    DataSet ds4 = new DataSet();
                    da4.Fill(ds4);

                    // convert the results into strings for comparision
                    string m1 = Convert.ToString(ds4.Tables[0].Rows[0]["M1"]);
                    string m2 = Convert.ToString(ds4.Tables[0].Rows[0]["M2"]);
                    string m3 = Convert.ToString(ds4.Tables[0].Rows[0]["M3"]);
                    string m4 = Convert.ToString(ds4.Tables[0].Rows[0]["M4"]);
                    string m5 = Convert.ToString(ds4.Tables[0].Rows[0]["M5"]);


                    if (m1 == null || m1 == "") {
                        // send message if free space
                        SqlCeCommand cmd3 = new SqlCeCommand("UPDATE Message set M1=@m WHERE Username = @u", con);
                        cmd3.Parameters.AddWithValue("@u", ContactsName);
                        cmd3.Parameters.AddWithValue("@m", message);
                        cmd3.ExecuteNonQuery();
                    } else if (m2 == null || m2 == "") {
                        // send message if free space
                        SqlCeCommand cmd3 = new SqlCeCommand("UPDATE Message set M2=@m WHERE Username = @u", con);
                        cmd3.Parameters.AddWithValue("@u", ContactsName);
                        cmd3.Parameters.AddWithValue("@m", message);
                        cmd3.ExecuteNonQuery();
                    } else if (m3 == null || m3 == "") {
                        // send message if free space
                        SqlCeCommand cmd3 = new SqlCeCommand("UPDATE Message set M3=@m WHERE Username = @u", con);
                        cmd3.Parameters.AddWithValue("@u", ContactsName);
                        cmd3.Parameters.AddWithValue("@m", message);
                        cmd3.ExecuteNonQuery();
                    } else if (m4 == null || m4 == "") {
                        // send message if free space
                        SqlCeCommand cmd3 = new SqlCeCommand("UPDATE Message set M4=@m WHERE Username = @u", con);
                        cmd3.Parameters.AddWithValue("@u", ContactsName);
                        cmd3.Parameters.AddWithValue("@m", message);
                        cmd3.ExecuteNonQuery();
                    } else if (m5 == null || m5 == "") {
                        // send message if free space
                        SqlCeCommand cmd3 = new SqlCeCommand("UPDATE Message set M5=@m WHERE Username = @u", con);
                        cmd3.Parameters.AddWithValue("@u", ContactsName);
                        cmd3.Parameters.AddWithValue("@m", message);
                        cmd3.ExecuteNonQuery();
                    } // if it get past here then there is no room and the message is not sent

                    // Close the SQL Connection
                    con.Close();
                }

                //if logged in remove the not logged in message
                lblStatus.Visible = false;

            } catch {
                // set login error
                lblStatus.Visible = true;
            }

        }

        protected void btnBlockUser_Click(object sender, EventArgs e) {
            try {
                // Get the Username from the Cookies
                string CookyUsername = Request.Cookies["Username"].Value;

                // Run SQL Query to get list of blocked Users
                DataSet ds = new DataSet();
                ds = SQLSearch.Username("SELECT * FROM Block WHERE Username = @u", CookyUsername);

                // convert the results into strings for comparision
                string B1 = Convert.ToString(ds.Tables[0].Rows[0]["B1"]);
                string B2 = Convert.ToString(ds.Tables[0].Rows[0]["B2"]);
                string B3 = Convert.ToString(ds.Tables[0].Rows[0]["B3"]);
                string B4 = Convert.ToString(ds.Tables[0].Rows[0]["B4"]);
                string B5 = Convert.ToString(ds.Tables[0].Rows[0]["B5"]);

                // Get the User that should be blocked
                string blockeduser = tbBlockUserName.Text;

                //Open A SQL connection
                SqlCeConnection con = new SqlCeConnection("Data Source=|DataDirectory|\\Database1.sdf; Persist Security Info=False");
                con.Open();

                if (B1 == null || B1 == "") {
                    // Run a SQL Update query if there is no name exists for this column
                    SqlCeCommand cmd3 = new SqlCeCommand("UPDATE Block set B1=@b WHERE Username = @u", con);
                    cmd3.Parameters.AddWithValue("@u", CookyUsername);
                    cmd3.Parameters.AddWithValue("@b", blockeduser);
                    cmd3.ExecuteNonQuery();

                } else if (B2 == null || B2 == "") {
                    // Run a SQL Update query if there is no name exists for this column
                    SqlCeCommand cmd3 = new SqlCeCommand("UPDATE Block set B2=@b WHERE Username = @u", con);
                    cmd3.Parameters.AddWithValue("@u", CookyUsername);
                    cmd3.Parameters.AddWithValue("@b", blockeduser);
                    cmd3.ExecuteNonQuery();

                } else if (B3 == null || B3 == "") {
                    // Run a SQL Update query if there is no name exists for this column
                    SqlCeCommand cmd3 = new SqlCeCommand("UPDATE Block set B3=@b WHERE Username = @u", con);
                    cmd3.Parameters.AddWithValue("@u", CookyUsername);
                    cmd3.Parameters.AddWithValue("@b", blockeduser);
                    cmd3.ExecuteNonQuery();

                } else if (B4 == null || B4 == "") {
                    // Run a SQL Update query if there is no name exists for this column
                    SqlCeCommand cmd3 = new SqlCeCommand("UPDATE Block set B4=@b WHERE Username = @u", con);
                    cmd3.Parameters.AddWithValue("@u", CookyUsername);
                    cmd3.Parameters.AddWithValue("@b", blockeduser);
                    cmd3.ExecuteNonQuery();

                } else if (B5 == null || B5 == "") {
                    // Run a SQL Update query if there is no name exists for this column
                    SqlCeCommand cmd3 = new SqlCeCommand("UPDATE Block set B5=@b WHERE Username = @u", con);
                    cmd3.Parameters.AddWithValue("@u", CookyUsername);
                    cmd3.Parameters.AddWithValue("@b", blockeduser);
                    cmd3.ExecuteNonQuery();

                }
                // Close the SQL Connection
                con.Close();

                //if logged in remove the not logged in message
                lblStatus.Visible = false;

            } catch {
                // set login error
                lblStatus.Visible = true;
            }
        }

        // function to show Messages
        public void showMessages() {

            //display textboxes with info in them only
            if (tbM1.Text == "" || tbM1.Text == null) {
                lblM1.Visible = false;
                tbM1.Visible = false;
                lblNOMSG.Visible = true;
            } else {
                lblM1.Visible = true;
                tbM1.Visible = true;
                lblNOMSG.Visible = false;
            }

            if (tbM2.Text == "" || tbM2.Text == null) {
                lblM2.Visible = false;
                tbM2.Visible = false;
            } else {
                lblM2.Visible = true;
                tbM2.Visible = true;
            }

            if (tbM3.Text == "" || tbM3.Text == null) {
                lblM3.Visible = false;
                tbM3.Visible = false;
            } else {
                lblM3.Visible = true;
                tbM3.Visible = true;
            }

            if (tbM4.Text == "" || tbM4.Text == null) {
                lblM4.Visible = false;
                tbM4.Visible = false;
            } else {
                lblM1.Visible = true;
                tbM1.Visible = true;
            }

            if (tbM5.Text == "" || tbM5.Text == null) {
                lblM5.Visible = false;
                tbM5.Visible = false;
            } else {
                lblM5.Visible = true;
                tbM5.Visible = true;
            }

        }

        // function to show Blocked Users
        public void showblocklist() {
            if (lblB1.Text == "" || lblB1.Text == null) {
                lblB1.Visible = false;
            } else {
                lblB1.Visible = true;
            }

            if (lblB2.Text == "" || lblB2.Text == null) {
                lblB2.Visible = false;
            } else {
                lblB2.Visible = true;
            }

            if (lblB3.Text == "" || lblB3.Text == null) {
                lblB3.Visible = false;
            } else {
                lblB3.Visible = true;
            }

            if (tbM4.Text == "" || tbM4.Text == null) {
                lblB4.Visible = false;
            } else {
                lblB1.Visible = true;
            }

            if (tbM5.Text == "" || tbM5.Text == null) {
                lblB5.Visible = false;
            } else {
                lblB5.Visible = true;
            }

        }

        // Show Main Menu
        public void showMainButtons() {
            btnMessageExport.Visible = true;
            btnMessageRefresh.Visible = true;
            btnShowBlockedUsersList.Visible = true;
            btnShowSendMessage.Visible = true;
            btnDeleteMessages.Visible = true;
        }

        // Show Message Buttons and Fields
        public void showMessgestuff() {
            lblentermsg.Visible = true;
            lblsendmsg.Visible = true;
            tbContactUsername.Visible = true;
            tbUserMessage.Visible = true;
            btnSendMessage.Visible = true;
        }

        protected void btnRefreshBlockUsers_Click(object sender, EventArgs e) {
            try {
                // Get the Username from the Cookies
                string CookyUsername = Request.Cookies["Username"].Value;

                DataSet ds = new DataSet();
                ds = SQLSearch.Username("SELECT B1,B2,B3,B4,B5 FROM Block WHERE Username = @u", CookyUsername);

                // Copy the Data to the textboxes
                lblB1.Text = Convert.ToString(ds.Tables[0].Rows[0]["B1"]);
                lblB2.Text = Convert.ToString(ds.Tables[0].Rows[0]["B2"]);
                lblB3.Text = Convert.ToString(ds.Tables[0].Rows[0]["B3"]);
                lblB4.Text = Convert.ToString(ds.Tables[0].Rows[0]["B4"]);
                lblB5.Text = Convert.ToString(ds.Tables[0].Rows[0]["B5"]);

                showblocklist();

            } catch {
                // set login error
                lblStatus.Visible = true;
            }
        }

        protected void btnDeleteMessages_Click(object sender, EventArgs e) {
            try {
                // Get the Username from the Cookies
                string CookyUsername = Request.Cookies["Username"].Value;

                // Run a SQL query to get a list of the Messages and copy to a Data Set
                SQLSearch.ClearMessageOrBlock("UPDATE Message set M1=@b WHERE Username = @u", CookyUsername);
                SQLSearch.ClearMessageOrBlock("UPDATE Message set M2=@b WHERE Username = @u", CookyUsername);
                SQLSearch.ClearMessageOrBlock("UPDATE Message set M3=@b WHERE Username = @u", CookyUsername);
                SQLSearch.ClearMessageOrBlock("UPDATE Message set M4=@b WHERE Username = @u", CookyUsername);
                SQLSearch.ClearMessageOrBlock("UPDATE Message set M5=@b WHERE Username = @u", CookyUsername);

                //if logged in remove the not logged in message
                lblStatus.Visible = false;

            } catch {
                // set login error
                lblStatus.Visible = true;
            }

        }

        protected void btnDeleteBlockedUsers_Click(object sender, EventArgs e) {
            try {
                // Get the Username from the Cookies
                string CookyUsername = Request.Cookies["Username"].Value;

                SQLSearch.ClearMessageOrBlock("UPDATE Block set B1=@b WHERE Username = @u", CookyUsername);
                SQLSearch.ClearMessageOrBlock("UPDATE Block set B2=@b WHERE Username = @u", CookyUsername);
                SQLSearch.ClearMessageOrBlock("UPDATE Block set B3=@b WHERE Username = @u", CookyUsername);
                SQLSearch.ClearMessageOrBlock("UPDATE Block set B4=@b WHERE Username = @u", CookyUsername);
                SQLSearch.ClearMessageOrBlock("UPDATE Block set B5=@b WHERE Username = @u", CookyUsername);

                //if logged in remove the not logged in message
                lblStatus.Visible = false;

            } catch {
                // set login error
                lblStatus.Visible = true;
            }

        }

    }

}

